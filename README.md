# Serverless Sample Application


[![coverage report](https://gitlab.com/patoncrispy/serverless-sample-app/badges/develop/coverage.svg)](https://gitlab.com/patoncrispy/serverless-sample-app/commits/develop) [![build status](https://gitlab.com/patoncrispy/serverless-sample-app/badges/develop/build.svg)](https://gitlab.com/patoncrispy/serverless-sample-app/commits/develop)

## Introduction

This application is basic example of what you can do with the Serverless Framework. Specifically, this application is designed to demonstrate a loosely coupled architecture when integrating with AWS services like S3, API Gateway or SNS. The idea is simple: integration points should be dumb and simply parse data from AWS services and pass them on to specific microservices elsewhere.

## Setup

If you're using GitLab CI, you'll need to have .env files for service settings. 


## TODO 
- [ ] Add integration tests.
- [ ] Add unit tests.