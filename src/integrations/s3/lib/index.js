'use strict';

const AWS = require('aws-sdk');

module.exports = class Lambda {
  constructor() {
    // TODO - Add apiVersion to config.
    this.lambda = new AWS.Lambda({ apiVersion: '2015-03-31' });
  }

  invokeFunction(arn, data, async) {
    return new Promise((resolve, reject) => {
      if (typeof data !== 'object' || typeof data !== 'string') {
        return reject({ message: 'data provided is invalid.' });
      }

      const params = {
        FunctionName: arn,
        InvocationType: async ? 'Event' : 'RequestResponse',
        Payload: typeof data === 'object' ? JSON.stringify(data) : data,
      };

      this.lambda.invoke(params, (error, result) => {
        if (error) {
          reject(error);
        } else {
          console.log(result);
          resolve(result);
        }
      });
    });
  }
};
